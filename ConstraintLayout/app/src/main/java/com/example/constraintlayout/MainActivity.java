package com.example.constraintlayout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CODE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void cancel(View view) {
        finish();
    }

    public void go(View view) {

        EditText name = findViewById(R.id.editTextTextPersonName);
        EditText id = findViewById(R.id.editTextTextPersonName2);
        String stringName = name.getText().toString();
        String stringPhone = id.getText().toString();

        Intent i = new Intent(this, MainActivity2.class);
        i.putExtra("yourName", stringName);
        i.putExtra("yourID",stringPhone);
        startActivityForResult(i, REQUEST_CODE);
    }

}